package za.co.marvin.mkhabela.tshwane.city.loadshedding.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;
import za.co.marvin.mkhabela.tshwane.city.loadshedding.model.GroupView;
import za.co.marvin.mkhabela.tshwane.city.loadshedding.model.ScheduleView;
import za.co.marvin.mkhabela.tshwane.city.loadshedding.service.LoadSheddingService;

import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class LoadSheddingApiController {

    private final LoadSheddingService loadSheddingService;

    @GetMapping(value = "/groups", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<GroupView>> getGroupListings() {
        return ResponseEntity.ok(loadSheddingService.getGroupListings());
    }

    @GetMapping(value = "/groups/{code}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GroupView> getGroupDetails(@NonNull @PathVariable("code") final int groupCode) {
        return loadSheddingService.getGroupDetails(groupCode)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping(value = "/groups/area/{area}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GroupView> getGroupByArea(@NonNull @PathVariable("area") final String areaName) {
        return loadSheddingService.getGroupByArea(areaName)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping(value = "/schedules/today", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ScheduleView>> getScheduleToday() {
        return ResponseEntity.ok(loadSheddingService.getScheduleToday());
    }

    @GetMapping(value = "/schedules/today/stage/{stage}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ScheduleView>> getScheduleTodayForStage(@NonNull @PathVariable("stage") final int stage) {
        return ResponseEntity.ok(loadSheddingService.getScheduleTodayForStage(stage));
    }

    @GetMapping(value = "/schedules/today/group/{group}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ScheduleView>> getScheduleTodayByGroup(@NonNull @PathVariable("group") final int groupCode) {
        return ResponseEntity.ok(loadSheddingService.getScheduleTodayByGroup(groupCode));
    }

    @GetMapping(value = "/schedules/group/{group}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ScheduleView>> getScheduleForGroup(@NonNull @PathVariable("group") final int groupCode) {
        return ResponseEntity.ok(loadSheddingService.getScheduleForGroup(groupCode));
    }

    @GetMapping(value = "/schedules/day/{day}/stage/{stage}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ScheduleView>> getScheduleForStageAndDay(@NonNull @PathVariable("stage") final int stage,
                                                                        @NonNull @PathVariable("day") final int dayOfTheMonth) {
        return ResponseEntity.ok(loadSheddingService.getScheduleForStageAndDay(stage, dayOfTheMonth));
    }

    @GetMapping(value = "/schedules/day/{day}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ScheduleView>> getScheduleForDay(@NonNull @PathVariable("day") final int dayOfTheMonth) {
        return ResponseEntity.ok(loadSheddingService.getScheduleForDay(dayOfTheMonth));
    }

    @GetMapping(value = "/schedules/group/{group}/stage/{stage}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ScheduleView>> getScheduleForStageByGroup(@NonNull @PathVariable("stage") final int stage,
                                                                         @NonNull @PathVariable("group") final int groupCode) {
        return ResponseEntity.ok(loadSheddingService.getScheduleForStageByGroup(stage, groupCode));
    }

    @GetMapping(value = "/schedules/day/{day}/group/{group}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ScheduleView>> getScheduleForDayByGroup(@NonNull @PathVariable("day") final int dayOfTheMonth,
                                                                       @NonNull @PathVariable("group") final int groupCode) {
        return ResponseEntity.ok(loadSheddingService.getScheduleForDayByGroup(dayOfTheMonth, groupCode));
    }

    @GetMapping(value = "/schedules/day/{day}/group/{group}/stage/{stage}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ScheduleView>> getScheduleForDayStageAndGroup(@NonNull @PathVariable("stage") final int stage,
                                                                             @NonNull @PathVariable("day") final int dayOfTheMonth,
                                                                             @NonNull @PathVariable("group") final int groupCode) {
        return ResponseEntity.ok(loadSheddingService
                .getScheduleForDayStageAndGroup(stage, dayOfTheMonth, groupCode));
    }

    @GetMapping(value = "/schedules/area/{area}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ScheduleView>> getScheduleTodayByArea(@NonNull @RequestParam("area") final String areaName) {
        return ResponseEntity.ok(loadSheddingService.getScheduleTodayByArea(areaName));
    }

    @GetMapping(value = "/schedules/area/{area}/stage/{stage}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ScheduleView>> getScheduleForStageByArea(@NonNull @RequestParam("stage") final int stage,
                                                                        @NonNull @RequestParam("area") final String areaName) {
        return ResponseEntity.ok(loadSheddingService.getScheduleForStageByArea(stage, areaName));
    }

    @GetMapping(value = "/schedules/day/{day}/area/{area}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ScheduleView>> getScheduleForDayByArea(@NonNull @RequestParam("day") final int dayOfTheMonth,
                                                                      @NonNull @RequestParam("area") final String areaName) {
        return ResponseEntity.ok(loadSheddingService.getScheduleForDayByArea(dayOfTheMonth, areaName));
    }

    @GetMapping(value = "/schedules/day/{day}/area/{area}/stage/{stage}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ScheduleView>> getScheduleForDayStageAndArea(@NonNull @RequestParam("stage") final int stage,
                                                                            @NonNull @RequestParam("day") final int dayOfTheMonth,
                                                                            @NonNull @RequestParam("area") final String areaName) {
        return ResponseEntity.ok(loadSheddingService
                .getScheduleForDayStageAndArea(stage, dayOfTheMonth, areaName));
    }
}
