package za.co.marvin.mkhabela.tshwane.city.loadshedding.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import za.co.marvin.mkhabela.tshwane.city.loadshedding.model.GroupView;
import za.co.marvin.mkhabela.tshwane.city.loadshedding.model.ScheduleView;
import za.co.marvin.mkhabela.tshwane.city.loadshedding.service.LoadSheddingService;

import java.util.Comparator;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
public class LoadSheddingController {

    private final LoadSheddingService loadSheddingService;

    @GetMapping(value = {"/", "/home"})
    public String getMainPage(final Model model) {
        model.addAttribute("groups", loadSheddingService.getGroupListings()
                .stream()
                .sorted(Comparator.comparing(GroupView::getGroupCode))
                .collect(Collectors.toList()));
        return "Home.html";
    }



    @GetMapping("/groups")
    public String getGroupListing(final Model model) {
        model.addAttribute("groups", loadSheddingService.getGroupListings()
                .stream()
                .sorted(Comparator.comparing(GroupView::getGroupCode))
                .collect(Collectors.toList()));
        return "Group";
    }

    @GetMapping("/schedule")
    public String getFullSchedule(final Model model) {
        model.addAttribute("schedules", loadSheddingService.getFullSchedule()
                .stream()
                .sorted(Comparator
                        .comparing(ScheduleView::getGroupCode)
                        .thenComparing(ScheduleView::getDayOfTheMonth)
                        .thenComparing(ScheduleView::getStage)
                )
                .collect(Collectors.toList()));
        return "Schedule";
    }

    @GetMapping("/schedule/today")
    public String getScheduleToday(final Model model) {
        model.addAttribute("schedules", loadSheddingService.getScheduleToday()
                .stream()
                .sorted(Comparator
                        .comparing(ScheduleView::getGroupCode)
                        .thenComparing(ScheduleView::getDayOfTheMonth)
                        .thenComparing(ScheduleView::getStage)
                )
                .collect(Collectors.toList()));
        return "Schedule";
    }

    @GetMapping("/schedule/group/{groupCode}")
    public String getScheduleByGroup(@PathVariable("groupCode") final int groupCode,
                              final Model model) {
        model.addAttribute("schedules", loadSheddingService.getScheduleForGroup(groupCode)
                .stream()
                .sorted(Comparator
                        .comparing(ScheduleView::getDayOfTheMonth)
                        .thenComparing(ScheduleView::getStage)
                )
                .collect(Collectors.toList()));
        return "Schedule";
    }

    @GetMapping("/schedule/today/group/{groupCode}")
    public String getScheduleTodayForGroup(@PathVariable("groupCode") final int groupCode,
                              final Model model) {
        model.addAttribute("schedules", loadSheddingService.getScheduleTodayByGroup(groupCode)
                .stream()
                .sorted(Comparator
                        .comparing(ScheduleView::getDayOfTheMonth)
                        .thenComparing(ScheduleView::getStage)
                )
                .collect(Collectors.toList()));
        return "Schedule";
    }
}
