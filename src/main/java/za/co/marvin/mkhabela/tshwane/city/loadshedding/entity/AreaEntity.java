package za.co.marvin.mkhabela.tshwane.city.loadshedding.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@Table(name = "areas")
public class AreaEntity implements Serializable{

    @Id
    @Column(name = "ID", nullable = false, unique = true)
    private long id;

    @Column(name = "NAME", nullable = false, unique = true)
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "GROUP_CODE", referencedColumnName = "CODE", nullable = false)
    private GroupEntity group;
}
