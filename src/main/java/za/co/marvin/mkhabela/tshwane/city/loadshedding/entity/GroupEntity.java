package za.co.marvin.mkhabela.tshwane.city.loadshedding.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@Table(name = "groups")
public class GroupEntity implements Serializable {

    @Id
    @Column(name = "ID", nullable = false, unique = true)
    private long id;

    @NaturalId
    @Column(name = "CODE", nullable = false, unique = true)
    private int code;

    @OneToMany(mappedBy = "group", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<AreaEntity> areas;
}
