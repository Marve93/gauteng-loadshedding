package za.co.marvin.mkhabela.tshwane.city.loadshedding.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@Table(name = "schedule")
public class ScheduleEntity implements Serializable {

    @Id
    @Column(name = "ID", nullable = false)
    private long id;

    @Column(name = "DAY_OF_THE_MONTH", nullable = false)
    private int dayOfTheMonth;

    @Column(name = "STAGE", nullable = false)
    private int stage;

    @Column(name = "START_TIME", nullable = false)
    private Time startTime;

    @Column(name = "END_TIME", nullable = false)
    private Time endTime;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "GROUP_CODE", referencedColumnName = "CODE", nullable = false)
    private GroupEntity group;
}
