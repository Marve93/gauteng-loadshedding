package za.co.marvin.mkhabela.tshwane.city.loadshedding.entity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import za.co.marvin.mkhabela.tshwane.city.loadshedding.entity.AreaEntity;

import java.util.Optional;

@Repository
public interface AreaRepository extends JpaRepository<AreaEntity, Long> {

    Optional<AreaEntity> findByName(final String name);
}
