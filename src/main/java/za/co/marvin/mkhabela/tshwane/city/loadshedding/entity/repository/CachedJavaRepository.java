package za.co.marvin.mkhabela.tshwane.city.loadshedding.entity.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import za.co.marvin.mkhabela.tshwane.city.loadshedding.entity.AreaEntity;
import za.co.marvin.mkhabela.tshwane.city.loadshedding.entity.GroupEntity;
import za.co.marvin.mkhabela.tshwane.city.loadshedding.entity.ScheduleEntity;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// Abstracts the database layer as Heroku free dyno deployment gets sluggish.

@Component
@Transactional
public class CachedJavaRepository {


    private final AreaRepository areaRepository;
    private final GroupRepository groupRepository;
    private final ScheduleRepository scheduleRepository;

    private Map<String, AreaEntity> areas;
    private Map<Integer, GroupEntity> groups;
    private Map<Integer, List<ScheduleEntity>> schedules;

    public CachedJavaRepository(final AreaRepository areaRepository,
                                final GroupRepository groupRepository,
                                final ScheduleRepository scheduleRepository) {
        this.areaRepository = areaRepository;
        this.groupRepository = groupRepository;
        this.scheduleRepository = scheduleRepository;

        refreshDatabaseEntries();
    }

    public List<AreaEntity> findAllAreas() {
        return new ArrayList<>(areas.values());
    }

    public Optional<AreaEntity> findAreaByName(final String name) {
        final AreaEntity areaEntity = areas.get(name);
        return Objects.isNull(areaEntity) ? Optional.empty() : Optional.of(areaEntity);
    }

    public List<GroupEntity> findAllGroups() {
        return new ArrayList<>(groups.values());
    }

    public Optional<GroupEntity> findGroupByCode(final int groupCode) {
        final GroupEntity groupEntity = groups.get(groupCode);
        return Objects.isNull(groupEntity) ? Optional.empty() : Optional.of(groupEntity);
    }

    public List<ScheduleEntity> findAllSchedules() {
        return getScheduleEntityStream()
                .collect(Collectors.toList());
    }

    public List<ScheduleEntity> findScheduleByDayOfTheMonth(final int dayOfTheMonth) {
        return schedules.get(dayOfTheMonth);
    }

    public List<ScheduleEntity> findScheduleByDayOfTheMonthAndGroup(final int dayOfTheMonth, final int groupCode) {
        return getScheduleEntityStream()
                .filter(scheduleEntity ->
                    scheduleEntity.getDayOfTheMonth() == dayOfTheMonth
                        && scheduleEntity.getGroup().getCode() == groupCode
                )
                .collect(Collectors.toList());
    }

    public List<ScheduleEntity> findScheduleByDayOfTheMonthAndStage(final int dayOfTheMonth, final int stage) {
        return getScheduleEntityStream()
                .filter(scheduleEntity ->
                        scheduleEntity.getDayOfTheMonth() == dayOfTheMonth
                                && scheduleEntity.getStage() == stage
                )
                .collect(Collectors.toList());
    }

    public List<ScheduleEntity> findScheduleByGroup(final int groupCode) {
        return getScheduleEntityStream()
                .filter(scheduleEntity -> scheduleEntity.getGroup().getCode() == groupCode)
                .collect(Collectors.toList());
    }

    public List<ScheduleEntity> findScheduleByGroupAndStage(final int groupCode, final int stage) {
        return getScheduleEntityStream()
                .filter(scheduleEntity ->
                    scheduleEntity.getGroup().getCode() == groupCode
                        && scheduleEntity.getStage() == stage
                )
                .collect(Collectors.toList());
    }

    public List<ScheduleEntity> findScheduleByDayOfTheMonthAndGroupAndStage(final int dayOfTheMonth,
                                                             final int groupCode,
                                                             final int stage) {
        return getScheduleEntityStream()
                .filter(scheduleEntity ->
                    scheduleEntity.getDayOfTheMonth() == dayOfTheMonth
                        && scheduleEntity.getGroup().getCode() == groupCode
                        && scheduleEntity.getStage() == stage
                )
                .collect(Collectors.toList());
    }

    @Scheduled(cron = "0 0 * * Sat", zone = "Africa/Johannesburg")
    public void refreshDatabaseEntries() {
        areas = areaRepository.findAll().stream().collect(Collectors.toMap(AreaEntity::getName, Function.identity()));
        groups = groupRepository.findAll().stream().collect(Collectors.toMap(GroupEntity::getCode, Function.identity()));
        schedules = scheduleRepository.findAll().stream().collect(Collectors.groupingBy(ScheduleEntity::getDayOfTheMonth));
    }

    private Stream<ScheduleEntity> getScheduleEntityStream() {
        return schedules.values()
                .stream()
                .flatMap(Collection::stream);
    }
}
