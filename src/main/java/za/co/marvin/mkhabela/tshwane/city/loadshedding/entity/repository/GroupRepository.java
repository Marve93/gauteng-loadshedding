package za.co.marvin.mkhabela.tshwane.city.loadshedding.entity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import za.co.marvin.mkhabela.tshwane.city.loadshedding.entity.GroupEntity;

import java.util.Optional;

@Repository
public interface GroupRepository extends JpaRepository<GroupEntity, Long> {

    Optional<GroupEntity> findByCode(final long code);
}
