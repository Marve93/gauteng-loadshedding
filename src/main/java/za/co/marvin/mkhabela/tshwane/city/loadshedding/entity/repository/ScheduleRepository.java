package za.co.marvin.mkhabela.tshwane.city.loadshedding.entity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import za.co.marvin.mkhabela.tshwane.city.loadshedding.entity.ScheduleEntity;

import java.util.List;

@Repository
public interface ScheduleRepository extends JpaRepository<ScheduleEntity, Long> {

    List<ScheduleEntity> findByDayOfTheMonth(final int dayOfTheMonth);

    // @Query(value = "FROM ScheduleEntity s WHERE s.stage = :stage AND s.dayOfTheMonth = :dayOfTheMonth")
    List<ScheduleEntity> findByStageAndDayOfTheMonth(final int stage, final int dayOfTheMonth);

    @Query(value = "FROM ScheduleEntity s WHERE s.group.code = :groupCode")
    List<ScheduleEntity> findByGroup(@Param("groupCode") final int groupCode);

    @Query(value = "FROM ScheduleEntity s WHERE s.group.code = :groupCode AND s.stage = :stage")
    List<ScheduleEntity> findByGroupAndStage(@Param("groupCode") final int groupCode,
                                             @Param("stage") final int stage);

    @Query(value = "FROM ScheduleEntity s " +
            "WHERE s.group.code = :groupCode AND s.stage = :stage AND s.dayOfTheMonth = :dayOfTheMonth")
    List<ScheduleEntity> findByGroupAndStageAndDayOfTheMonth(@Param("groupCode") final int groupCode,
                                                             @Param("stage") final int stage,
                                                             @Param("dayOfTheMonth") final int dayOfTheMonth);

    @Query(value = "FROM ScheduleEntity s WHERE s.group.code = :groupCode AND s.dayOfTheMonth = :dayOfTheMonth")
    List<ScheduleEntity> findByGroupAndDayOfTheMonth(@Param("groupCode") final int groupCode,
                                                     @Param("dayOfTheMonth") final int dayOfTheMonth);
}
