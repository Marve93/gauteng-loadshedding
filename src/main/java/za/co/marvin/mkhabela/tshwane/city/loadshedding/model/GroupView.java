package za.co.marvin.mkhabela.tshwane.city.loadshedding.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class GroupView {

    private int groupCode;

    private List<String> areas;
}
