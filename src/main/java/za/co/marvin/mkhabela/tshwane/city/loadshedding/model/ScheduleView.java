package za.co.marvin.mkhabela.tshwane.city.loadshedding.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalTime;
import java.util.Objects;

@Getter
@Setter
@Accessors(chain = true)
public class ScheduleView {

    private int stage;

    private GroupView group;

    private int dayOfTheMonth;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    private LocalTime startTime;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    private LocalTime endTime;

    public int getGroupCode() {
        return Objects.isNull(group) ? 0 : group.getGroupCode();
    }
}
