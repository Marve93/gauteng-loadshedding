package za.co.marvin.mkhabela.tshwane.city.loadshedding.service;

import za.co.marvin.mkhabela.tshwane.city.loadshedding.model.GroupView;
import za.co.marvin.mkhabela.tshwane.city.loadshedding.model.ScheduleView;

import java.util.List;
import java.util.Optional;

public interface LoadSheddingService {

    Optional<GroupView> getGroupByArea(final String areaName);

    List<GroupView> getGroupListings();

    Optional<GroupView> getGroupDetails(final int groupCode);

    List<ScheduleView> getScheduleToday();

    List<ScheduleView> getFullSchedule();

    List<ScheduleView> getScheduleTodayForStage(final int stage);

//    List<ScheduleView> getScheduleByStage(final int stage);

    List<ScheduleView> getScheduleTodayByGroup(final int groupCode);

    List<ScheduleView> getScheduleForGroup(final int groupCode);

    List<ScheduleView> getScheduleForStageAndDay(final int stage,
                                                 final int dayOfTheMonth);

    List<ScheduleView> getScheduleForDay(final int dayOfTheMonth);

    List<ScheduleView> getScheduleForStageByGroup(final int stage,
                                                  final int groupCode);

    List<ScheduleView> getScheduleForDayByGroup(final int dayOfTheMonth,
                                                final int groupCode);

    List<ScheduleView> getScheduleForDayStageAndGroup(final int stage,
                                                      final int dayOfTheMonth,
                                                      final int groupCode);

    List<ScheduleView> getScheduleTodayByArea(final String areaName);

    List<ScheduleView> getScheduleForStageByArea(final int stage,
                                                 final String areaName);

    List<ScheduleView> getScheduleForDayByArea(final int dayOfTheMonth,
                                               final String areaName);

    List<ScheduleView> getScheduleForDayStageAndArea(final int stage,
                                                     final int dayOfTheMonth,
                                                     final String areaName);
}
