package za.co.marvin.mkhabela.tshwane.city.loadshedding.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.marvin.mkhabela.tshwane.city.loadshedding.entity.AreaEntity;
import za.co.marvin.mkhabela.tshwane.city.loadshedding.entity.GroupEntity;
import za.co.marvin.mkhabela.tshwane.city.loadshedding.entity.ScheduleEntity;
import za.co.marvin.mkhabela.tshwane.city.loadshedding.entity.repository.CachedJavaRepository;
import za.co.marvin.mkhabela.tshwane.city.loadshedding.model.GroupView;
import za.co.marvin.mkhabela.tshwane.city.loadshedding.model.ScheduleView;
import za.co.marvin.mkhabela.tshwane.city.loadshedding.util.DateUtil;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class LoadSheddingServiceImpl implements LoadSheddingService {

    private final CachedJavaRepository cachedJavaRepository;

    @Autowired
    public LoadSheddingServiceImpl(final CachedJavaRepository cachedJavaRepository) {
        this.cachedJavaRepository = cachedJavaRepository;
    }

    @Override
    public Optional<GroupView> getGroupByArea(final String areaName) {
        return cachedJavaRepository.findAreaByName(areaName)
                .map(area -> createGroupView(area.getGroup()));
    }

    @Override
    public List<GroupView> getGroupListings() {
        return cachedJavaRepository.findAllGroups()
                .stream()
                .map(this::createGroupView)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<GroupView> getGroupDetails(final int groupCode) {
        return cachedJavaRepository
                .findGroupByCode(groupCode)
                .map(this::createGroupView);
    }

    @Override
    public List<ScheduleView> getScheduleToday() {
        final int dayOfTheMonth = DateUtil.getCurrentDayInSouthAfrica();
        return cachedJavaRepository.findScheduleByDayOfTheMonth(dayOfTheMonth)
                .stream()
                .map(this::createScheduleView)
                .collect(Collectors.toList());
    }

    @Override
    public List<ScheduleView> getFullSchedule() {
        return cachedJavaRepository.findAllSchedules()
                .stream()
                .map(this::createScheduleView)
                .collect(Collectors.toList());
    }

    @Override
    public List<ScheduleView> getScheduleTodayForStage(final int stage) {
        final int dayOfTheMonth = DateUtil.getCurrentDayInSouthAfrica();
        return cachedJavaRepository.findScheduleByDayOfTheMonthAndStage(dayOfTheMonth, stage)
                .stream()
                .map(this::createScheduleView)
                .collect(Collectors.toList());
    }

    @Override
    public List<ScheduleView> getScheduleTodayByGroup(final int groupCode) {
        final int dayOfTheMonth = DateUtil.getCurrentDayInSouthAfrica();
        return cachedJavaRepository.findScheduleByDayOfTheMonthAndGroup(dayOfTheMonth, groupCode)
                .stream()
                .map(this::createScheduleView)
                .collect(Collectors.toList());
    }

    @Override
    public List<ScheduleView> getScheduleForGroup(final int groupCode) {
        return cachedJavaRepository.findScheduleByGroup(groupCode)
                .stream()
                .map(this::createScheduleView)
                .collect(Collectors.toList());
    }

    @Override
    public List<ScheduleView> getScheduleForStageAndDay(final int stage,
                                                        final int dayOfTheMonth) {
        return cachedJavaRepository.findScheduleByDayOfTheMonthAndStage(dayOfTheMonth, stage)
                .stream()
                .map(this::createScheduleView)
                .collect(Collectors.toList());
    }

    @Override
    public List<ScheduleView> getScheduleForDay(final int dayOfTheMonth) {
        return cachedJavaRepository.findScheduleByDayOfTheMonth(dayOfTheMonth)
                .stream()
                .map(this::createScheduleView)
                .collect(Collectors.toList());
    }

    @Override
    public List<ScheduleView> getScheduleForStageByGroup(final int stage,
                                                         final int groupCode) {
        return cachedJavaRepository.findScheduleByGroupAndStage(groupCode, stage)
                .stream()
                .map(this::createScheduleView)
                .collect(Collectors.toList());
    }

    @Override
    public List<ScheduleView> getScheduleForDayByGroup(final int dayOfTheMonth,
                                                       final int groupCode) {
        return cachedJavaRepository.findScheduleByDayOfTheMonthAndGroup(dayOfTheMonth, groupCode)
                .stream()
                .map(this::createScheduleView)
                .collect(Collectors.toList());
    }

    @Override
    public List<ScheduleView> getScheduleForDayStageAndGroup(final int stage,
                                                             final int dayOfTheMonth,
                                                             final int groupCode) {
        return cachedJavaRepository.findScheduleByDayOfTheMonthAndGroupAndStage(dayOfTheMonth, groupCode, stage)
                .stream()
                .map(this::createScheduleView)
                .collect(Collectors.toList());
    }

    @Override
    public List<ScheduleView> getScheduleTodayByArea(final String areaName) {
        return cachedJavaRepository.findAreaByName(areaName)
                .map(area -> getScheduleTodayByGroup(area.getGroup().getCode()))
                .orElse(new ArrayList<>());
    }

    @Override
    public List<ScheduleView> getScheduleForStageByArea(final int stage,
                                                        final String areaName) {
        return cachedJavaRepository.findAreaByName(areaName)
                .map(area -> getScheduleForStageByGroup(stage, area.getGroup().getCode()))
                .orElse(new ArrayList<>());
    }

    @Override
    public List<ScheduleView> getScheduleForDayByArea(final int dayOfTheMonth,
                                                      final String areaName) {
        return cachedJavaRepository.findAreaByName(areaName)
                .map(area -> getScheduleForDayByGroup(dayOfTheMonth, area.getGroup().getCode()))
                .orElse(new ArrayList<>());
    }

    @Override
    public List<ScheduleView> getScheduleForDayStageAndArea(final int stage,
                                                            final int dayOfTheMonth,
                                                            final String areaName) {
        return cachedJavaRepository.findAreaByName(areaName)
                .map(area -> getScheduleForDayStageAndGroup(stage, dayOfTheMonth, area.getGroup().getCode()))
                .orElse(new ArrayList<>());
    }

    private GroupView createGroupView(final GroupEntity groupEntity) {
        return new GroupView()
                .setGroupCode(groupEntity.getCode())
                .setAreas(groupEntity.getAreas()
                        .stream()
                        .map(AreaEntity::getName)
                        .sorted(Comparator.comparing(Function.identity()))
                        .collect(Collectors.toList()));
    }

    private ScheduleView createScheduleView(final ScheduleEntity scheduleEntity) {
        return new ScheduleView()
                .setStage(scheduleEntity.getStage())
                .setDayOfTheMonth(scheduleEntity.getDayOfTheMonth())
                .setGroup(this.createGroupView(scheduleEntity.getGroup()))
                .setStartTime(scheduleEntity.getStartTime().toLocalTime())
                .setEndTime(scheduleEntity.getEndTime().toLocalTime());
    }
}
