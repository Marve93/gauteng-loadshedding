package za.co.marvin.mkhabela.tshwane.city.loadshedding.util;

import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

public class DateUtil {

    private static ZoneId SOUTH_AFRICA_ZONE_ID = ZoneId.of(ZoneOffset.ofHours(2).getId());

    private DateUtil(){}

    public static int getCurrentDayInSouthAfrica() {
        final ZonedDateTime nowInSouthAfrica = getSouthAfricanTime();
        return nowInSouthAfrica.getDayOfMonth();
    }

    private static ZonedDateTime getSouthAfricanTime() {
        return ZonedDateTime.now(SOUTH_AFRICA_ZONE_ID);
    }
}
